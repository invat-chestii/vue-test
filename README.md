# test

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Pentru server local json
npm install -g json-server
### json-server --watch numefisier.json -r nume_fisier_rute.json --id nume_id_de_mapat_din_fisierul_cu_date
 json-server -w db.json -r routes.json --id guid
