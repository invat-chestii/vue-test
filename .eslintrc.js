module.exports = {
  root: true,

  env: {
    node: true
  },

  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],

  parserOptions: {
    parser: 'babel-eslint'
  },

  rules: {
    'no-console': 'off',
    'no-debugger': 'off'
  },

  'extends': [
    'plugin:vue/essential',
    '@vue/standard'
  ]
}



// module.exports = {
//   root: true,
//   env: {
//     node: true
//   },
//   plugins: ["import"],
//   extends: [
//     "plugin:vue/essential",
//     "eslint:recommended",
//     "@vue/prettier",
//     "plugin:import/errors",
//     "plugin:import/warnings"
//   ],
//   parserOptions: {
//     parser: "babel-eslint"
//   },
//   settings: {
//     "import/resolver": {
//       alias: {
//         map: [["@", "./src"]],
//         extensions: [".vue", ".json", ".js"]
//       }
//     }
//   },
//   rules: {
//     "import/no-cycle": 1,
//     "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
//     "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
//     "no-unused-vars": [
//       "warn",
//       {
//         ignoreRestSiblings: true
//       }
//     ]
//   },
//   overrides: [
//     {
//       files: [
//         "**/__tests__/*.{j,t}s?(x)",
//         "**/__mocks__/*.{j,t}s?(x)",
//         "**/tests/unit/**/*.{j,t}s?(x)",
//         "**/tests/unit/helpers.js"
//       ],
//       env: {
//         jest: true
//       }
//     }
//   ]
// };
