const state = {
    email: null
}

const getters = {
    userEmail: (state) => state.email,
}

const actions = {
    setEmail({commit}) {
        commit('SET_EMAIL', userEmail)
    }
}

const mutations = {
    SET_EMAIL: (state, email) => ( state.email = email )
}


export default {
    state,
    getters,
    actions,
    mutations
}