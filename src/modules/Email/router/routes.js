const emailRoutes = [
    {
      path: '/page-1',
      name: 'page-one',
      // beforeEnter(to, from, next) {
      //   console.log(to,from,next);
      //   next({ pageName: `${pageName}`});
      // },
      meta:{
        email: 'nope',
        userStatus: 'test'
      },
    //   component: () => import(/* webpackChunkName: "page-one" */ '../components/PageOne.vue')
      component: () => import(/* webpackChunkName: "page-one" */ '@/modules/Email/components/PageOne.vue')
    },
    {
      path: '/page-2',
      name: 'page-two',
      component: () => import(/* webpackChunkName: "page-two" */ '@/modules/Email/components/PageTwo.vue')
    }
]

export default emailRoutes
