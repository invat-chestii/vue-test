import store from './store/email.store'
import routes from './router/routes'

export default{
    name: 'EmailModule',
    store: store,
    routes: routes
}