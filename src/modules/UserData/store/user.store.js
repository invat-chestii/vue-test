const state = {
    isLoggedIn: null
}

const getters = {
    getLoggedStatus: (state) => state.isLoggedIn
}

const actions = {
    setLoggedStatus({commit}){
        console.log(userStatus);
        commit('SET_STATUS', userStatus)
    }
}

const mutations = {
    SET_STATUS: (state, userStatus) => (state.isLoggedIn = userStatus)
}


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}