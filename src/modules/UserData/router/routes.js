const areaRoutes = [    
    {
      path: '/area-home',
      name: 'area-home',
      component: () => import(/* webpackChunkName: "secure-page" */ '@/modules/UserData/views/AreaFityOne.vue'),
      meta: {
        requiresAuth: true
      }
    }
]

export default areaRoutes
