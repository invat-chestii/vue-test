import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import EmailStore from '../modules/Email/store/email.store'
import UserStore from '../modules/UserData/store/user.store'

// import { EmailStore } from '@/modules/Email/store/email.store'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    fakeData:null,
    loaded: false
  },
  getters: {
    getNames: state => {
      let userName = state.fakeData.map( item => item.name )
      return userName;
    },
    getUserData: state => {
      if (state.fakeData !== null){
        let userData = state.fakeData.map( item => {
          return {
            name: `${item.name.first} ${item.name.last}`,
            registered: `${item.registered}`,
            img: `${item.picture}`,
            about: `${item.about}`,
            // friends: `${item.friends.map( x => { x })}`,
            uid: `${item.guid}`,
          }
        })
        return userData
      }
    },
    getUserId: (state) => (userId) => {
      return state.fakeData.find( uid =>  uid.guid === userId)
    }
  },
  mutations: {
    SET_FAKEDATA (state, fakeData) {
        state.fakeData = fakeData
    },
    SET_LOADED (state, loaded){
      state.loaded = loaded
    },
    SET_USER(state, user){
      // console.log('from fake data', state.fakeData);
      let test = state.fakeData.find( uid =>  uid.guid === user.guid) 
      console.log(test)
      test = user
    },
    ADD_USER(state, user){
      console.log('from add user', user)
      state.fakeData.push(user)
    }
  },
  actions: {
    getData({ commit }){
      axios.get('http://localhost:3000/data',{
        headers: {"Content-type": "application/json; charset=UTF-8"}
      })
        .then(response => response.data)
        .then( fakeData => {
          commit('SET_FAKEDATA', fakeData)
        })
        .then(
          commit('SET_LOADED', true)
        )
        .catch(function (error) {
          if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log(error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error', error.message);
          }
          console.log(error.config);
        })
    },
    updateUser({commit}, user){
      
      let apiUrl = `http://localhost:3000/api/v1/data/${user.guid}`
  
      axios.put(apiUrl, user)
      .then( commit('SET_USER', user) )
    },
    addUser({commit}, user){
      axios.post('http://localhost:3000/data/', user)
      .then( commit('ADD_USER', user) )
    }
  },
  modules: {
    EmailModule: EmailStore,
    UserModule: UserStore
  },
})
