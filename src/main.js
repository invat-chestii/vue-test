import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router,
  // watch: {
  //   '$route'(to, from){
  //     console.log(from);
  //   }  
  // },
  store,
  render: h => h(App)
}).$mount('#app')
