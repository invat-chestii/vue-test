import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

import emailRoutes from '@/modules/Email/router/routes'
import areaRoutes from '@/modules/UserData/router/routes'

Vue.use(VueRouter)

const routes = [
  ...emailRoutes,
  ...areaRoutes,
  {
    path: '/',
    name: 'Home',
    component: Home,
    beforeEnter: (to, from, next) => {
      if(from.path === '/add-user'){
        console.log('test from router')
      } 
      if( from.params){
        console.log(from.params);
      }
      next()
    }
  },
  {
    path: '/edit-user/:id',
    name: 'edit-user',
    component: () => import(/* webpackChunkName: "edit-user" */ '@/components/UserEdit.vue')
  },
  {
    path: '/user/:id',
    name: 'user',
    component: () => import(/* webpackChunkName: "user-details" */ '@/components/UserDetails.vue')
  },
  {
    path: '/add-user',
    name: 'add-user',
    component: () => import(/* webpackChunkName: "add-new-user" */ '@/components/UserAddNewUser.vue'),

  },
  {
    path: '/add-email/:emailType',
    name: 'add-email',
    component: () => import(/* webpackChunkName: "add-user-email" */ '@/modules/Email/views/UserAddEmail.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  let isAuth = true
  if(to.matched.some( item => item.meta.requiresAuth )){
    
    if(isAuth){
      next()
    } else {
      router.replace('/Home')
    }

  } else {
    next()
  }

})

export default router
